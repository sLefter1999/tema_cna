#include <iostream>
#include <SumOperation.grpc.pb.h>
#include <PrintNameOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
    grpc_init();
    ClientContext context;
    auto sum_stub = PrintNameOperationService::NewStub(grpc::CreateChannel("localhost:8888",
        grpc::InsecureChannelCredentials()));
    PrintNameRequest nameRequest;
        std::cout << "Introduceti numele:";

        std::string name;

        std::cin >> name;

        nameRequest.set_clientname(name);
        Empty response;
        auto status = sum_stub->PrintName(&context, nameRequest, &response);
}
