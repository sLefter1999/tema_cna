#pragma once

#include "SumOperation.grpc.pb.h"
#include "PrintNameOperation.grpc.pb.h"

class AdditionServiceImpl final : public PrintNameOperationService::Service
{
public:
	AdditionServiceImpl() {};
     ::grpc::Status PrintName(::grpc::ServerContext* context, const ::PrintNameRequest* request, ::Empty* response) override;
};