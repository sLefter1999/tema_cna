#include "AdditionServiceImpl.h"

::grpc::Status AdditionServiceImpl::PrintName(::grpc::ServerContext* context, const::PrintNameRequest* request, ::Empty* response)
{
	std::string name = request->clientname();

	std::cout << name << std::endl;

	return ::grpc::Status::OK;
}
